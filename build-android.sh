#!/bin/bash

checkfail() {
    if [ ! $? -eq 0 ]; then
        echo -e "\e[01;31m!!! $1\e[0m" >&2
        exit 1
    fi
}

check() {
if ! $1 --version >/dev/null 2>&1 && ! $1 -version >/dev/null 2>&1
then
    echo "$1 not found"
    NEEDED="$NEEDED .$1"
else
    # found, need to check version ?
    [ -z "$2" ] && return # no
    gotver=`$1 --version | head -1 | sed s/'.* '//`
    gotmajor=`echo $gotver|cut -d. -f1`
    gotminor=`echo $gotver|cut -d. -f2`
    gotmicro=`echo $gotver|cut -d. -f3`
    [ -z "$gotmicro" ] && gotmicro=0
    needmajor=`echo $2|cut -d. -f1`
    needminor=`echo $2|cut -d. -f2`
    needmicro=`echo $2|cut -d. -f3`
    [ -z "$needmicro" ] && needmicro=0
    if [ "$needmajor" -gt "$gotmajor" \
         -o "$needmajor" -eq "$gotmajor" -a "$needminor" -gt "$gotminor" \
         -o "$needmajor" -eq "$gotmajor" -a "$needminor" -eq "$gotminor" -a "$needmicro" -gt "$gotmicro" ]
    then
        echo "$1 too old"
        NEEDED="$NEEDED .$1"
    fi
fi
}

# dpkg -l "*libname*"

# check tools
check autoconf 2.69
check automake 1.14
check m4 1.4.16
check libtool 2.4
check pkg-config
check cmake 3.4.3
check yasm
#check_tar
check ragel
#check_sed
check protoc 2.6.0
check ant
check xz

# iconv

#
# Source Setup
#
BASE_DIR=./contrib/contrib-android

VLC_VER=42e5542
VLC_DIR=vlc-$VLC_VER

VLC_ANDROID_VER=1.9.6
VLC_ANDROID_DIR=vlc-android-$VLC_ANDROID_VER

mkdir -p $BASE_DIR
pushd $BASE_DIR

# VLC Source
# clone from git
if [ ! -d "vlc.git" ]; then
    GIT_REPO=git://git.videolan.org/vlc.git
    echo "clone vlc source from $GIT_REPO"
    git clone $GIT_REPO vlc.git
    checkfail "vlc clone: failed"
fi
# export
if [ ! -d "$VLC_DIR" ]; then
    if [ ! -e "$VLC_DIR.tar.gz" ]; then
        cd vlc.git
        git archive $VLC_VER --format tar.gz -o ../$VLC_DIR.tar.gz
        checkfail "vlc export: git archive failed. VER: $VLC_VER"
        cd ..
    fi
    mkdir $VLC_DIR
    tar -xf $VLC_DIR.tar.gz -C $VLC_DIR
    # make fake git dir
    cd $VLC_DIR
    git init && git add README && git commit -m "start"
    cd ..
fi

# VLC_ANDROID source
# clone from git
if [ ! -d "vlc-android.git" ]; then
    GIT_REPO=https://code.videolan.org/videolan/vlc-android.git
    echo "clone vlc-android source from $GIT_REPO"
    git clone $GIT_REPO vlc-android.git
    checkfail "vlc-android clone: failed"
fi
# export
if [ ! -d "$VLC_ANDROID_DIR" ]; then
    if [ ! -e "$VLC_ANDROID_DIR.tar.gz" ]; then
        cd vlc-android.git
        git archive $VLC_ANDROID_VER --format tar.gz -o ../$VLC_ANDROID_DIR.tar.gz
        checkfail "vlc-android export: git archive failed. VER: $VLC_ANDROID_VER"
        cd ..
    fi
    mkdir $VLC_ANDROID_DIR
    tar -xf $VLC_ANDROID_DIR.tar.gz -C $VLC_ANDROID_DIR
fi

# make or update a symbolic link for vlc source
ln -sfn ../$VLC_DIR $VLC_ANDROID_DIR/vlc

echo ""
echo "Source done."
echo "#####################################"
echo ""


#
# Patch
#
pushd $VLC_ANDROID_DIR
PATCH_FILE=../../src/vlc-android/v1.9.6.patch
patch -p0 -N --dry-run --silent < $PATCH_FILE 2>/dev/null
#If the patch has not been applied then the $? which is the exit status 
#for last command would have a success status code = 0
if [ $? -eq 0 ]; then
    echo "Applying patch...: $PATCH_FILE"
    patch -p0 -N < $PATCH_FILE
    checkfail "vlc-android patch: failed..."
fi
popd #$VLC_ANDROID_DIR

echo ""
echo "Patch done."
echo "#####################################"
echo ""

#
# Build (REF: https://wiki.videolan.org/AndroidCompile/)
#
pushd $VLC_ANDROID_DIR

./compile.sh -a armeabi-v7a
checkfail "vlc-android build: failed..."

popd #$VLC_ANDROID_DIR

echo ""
echo "Build done."
echo "#####################################"
echo ""

popd #$BASE_DIR
