
#
# VLC Build (https://wiki.videolan.org/Compile_VLC/)
#

########################
## VLC-android

### Reference
    - Source: git clone https://code.videolan.org/videolan/vlc-android.git
    - Compile: https://wiki.videolan.org/AndroidCompile/
    - Dev. Doc: https://wiki.videolan.org/Android/
### Build
    - OS: CentOS 7
    - VLC: 42e5542
    - VLC-android: 1.9.6


########################
## VLC-iOS
    - VLCKit: git clone https://code.videolan.org/videolan/VLCKit.git
    - Source: http://www.videolan.org/vlc/download-ios.html
              2.7.2: http://get.videolan.org/vlc-iOS/2.7.2/vlc-ios-2.7.2.tar.gz
              MobileVLCKit 3.0.0-pre2: http://get.videolan.org/vlc-iOS/2.7.1/VLCKit-3.0.0-pre2.tar.gz
              2.2.2: https://code.videolan.org/videolan/VLCKit/repository/archive.zip?ref=2.2.2
    - Compile: https://wiki.videolan.org/IOSCompile/
    - Dev. Doc: https://wiki.videolan.org/IOS/


########################
## VLC-Win32
    - Compile: https://wiki.videolan.org/Win32Compile/


#
# VLC Hack
#
- Hacker Guide/VLC source tree: https://wiki.videolan.org/VLC_source_tree/
- Hacker Guide/Modules source tree: https://wiki.videolan.org/Modules_source_tree/

